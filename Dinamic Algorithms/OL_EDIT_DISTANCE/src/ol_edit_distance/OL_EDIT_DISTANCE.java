package ol_edit_distance;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class OL_EDIT_DISTANCE{

    static ArrayList<String> entryData;
    static ArrayList<String> outputData;
    
    static void readFile(String filename){
        entryData = new ArrayList<>();
        try{
            Scanner scanInput = new Scanner(new FileReader(filename));
            while(scanInput.hasNext()){
                entryData.add(scanInput.next());
            }            
        }catch(FileNotFoundException fnfe){
            System.out.println(fnfe);
            System.exit(1);
        }
    }//End of readFile
    
    static void saveFile(String arquivo){
        BufferedWriter output;
        try( FileWriter fw = new FileWriter(arquivo) ){
            output = new BufferedWriter(fw);
               
            for(int i=0;i<outputData.size();i++){
                output.append(outputData.get(i));
                output.newLine();
            }
            output.close();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }//End of saveFile
    
    static int min(int x, int y, int z){
        if (x < y && x <z){ return x;}
        if (y < x && y < z){ return y;}
        else return z;
    }//End of min
 
    static int editDist(String str1, String str2){
        int m = str1.length(), n = str2.length();
        int dp[][] = new int[m+1][n+1];
      
        for (int i=0; i<=m; i++)
        {
            for (int j=0; j<=n; j++)
            {
                if (i==0){
                    dp[i][j] = j; 
                }else if (j==0){
                    dp[i][j] = i; 
                }else if (str1.charAt(i-1) == str2.charAt(j-1)){
                    dp[i][j] = dp[i-1][j-1];
                }else{
                    dp[i][j] = 1 + min(dp[i][j-1],    // Insert
                                       dp[i-1][j],    // Remove
                                       dp[i-1][j-1]); // Replace
                }
            }
        }
        return dp[m][n];
    }//End of editDist
    
    public static void main(String[] args){
        readFile("entry.in");
        
        String result;
        outputData = new ArrayList<>();
        
        for(int i = 0; i < entryData.size();i++){
            result = entryData.get(i) + " " 
                     + entryData.get(i+1) + " " 
                     + editDist(entryData.get(i), entryData.get(i+1)) 
                     + " " + "passos";
            outputData.add(result);
            System.out.println(entryData.get(i) + " " +entryData.get(i+1) + " " 
                               + editDist( entryData.get(i), entryData.get(i+1)) 
                               + " " + "passos");
            i++;
        }
        saveFile("output.in");
    }//End of main
    
}
